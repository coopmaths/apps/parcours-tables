import App from './App.svelte'
import './app.css'
import 'boxicons/css/boxicons.min.css'

// Polyfill for Array.prototype.toSorted
if (!Array.prototype.toSorted) {
  // eslint-disable-next-line no-extend-native
  Array.prototype.toSorted = function (compareFunction) {
    const arrayCopy = this.slice()
    arrayCopy.sort(compareFunction)
    return arrayCopy
  }
}

const app = new App({
  target: document.getElementById('app')
})

export default app
