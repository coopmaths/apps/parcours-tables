function shuffleArray (array: any[]) {
  return array.toSorted(() => Math.random() - 0.5)
}

export default function shuffleAndConcat (array: any[], size: number) {
  let shuffled = shuffleArray(array)
  while (shuffled.length < size) {
    shuffled = shuffled.concat(shuffleArray(array))
  }
  return shuffled.slice(0, size)
}
