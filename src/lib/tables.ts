import shuffleAndConcat from './shuffleAndConcat'

export default function createQuestionsTables ({
  numberOfQuestions = 10,
  tables = new Set([2, 3, 4, 5, 6, 7, 8, 9, 10]),
  min = 2,
  max = 9,
  tableIsFirst
}: {numberOfQuestions?: number, tables?: Set<number>, min?: number, max?: number, tableIsFirst?: boolean } ) {
  const questions = []
  const tablesArray = Array.from(tables)
  const tablesMixed = shuffleAndConcat(tablesArray, numberOfQuestions)
  const numbersMixed = shuffleAndConcat(Array(max - min + 1).fill(0).map((_, i) => i + min), numberOfQuestions)
  let arrayOftableIsFirst: boolean[] = []
  if (tableIsFirst !== undefined) {
    arrayOftableIsFirst = Array(numberOfQuestions).fill(tableIsFirst)
  } else {
    arrayOftableIsFirst = shuffleAndConcat([true, false], numberOfQuestions)
  }
  for (let i = 0; i < numberOfQuestions; i++) {
    const table = tablesMixed[i]
    const number = numbersMixed[i]
    const text = arrayOftableIsFirst[i] ? `${table} × ${number} = ` : `${number} × ${table} = `
    const answer = table * number
    questions.push({ text, answer })
  }
  return questions
}
